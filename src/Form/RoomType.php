<?php

namespace App\Form;

use App\Entity\Room;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class,[
                'required' =>true,
                'label'=>false,
                'attr'=>[
                    'placeholder'=>'Nom de la salle',
                    'class'=>'FormClassTest'
                ]
            ])
            ->add('capacity', IntegerType::class)
            ->add('city',TextType::class)
            ->add('description', TextareaType::class,[
                'required'=>false,
                'constraints'=>[
                    new NotBlank([
                        'message'=>'Merci d\'ajouter une description'
                    ]),
                    new Length([
                        'min'=>10,
                        'max'=>600,
                        'minMessage'=>'Le message est pour lee min qui n\'est pas atteint',
                        'maxMessage'=>'Le message est pour lee max qui n\'est pas atteint'
                    ])
                ]
            ])
            // ->add('isAvailable')
            // ->add('user')
            ->add('created_at')
            ->add('updated_at')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
        ]);
    }
}
